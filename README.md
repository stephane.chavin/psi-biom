# PSI-BIOM

## Install

```bash
git clone https://gitlab.lis-lab.fr/stephane.chavin/psi-biom.git

```


<summary>Extraction des spectrogrammes</summary>
<p>
</p>

```bash
python3 get_spectrogram.py -f NAME_OF_YOUR_FILE.csv -p PATH_TO_THE_DATA -d DIRECTION_OF_SAVED_DATA -m {unique or multiple} -n NAME_OF_THE_COLUMNS_THAT_CONTAIN_PATH
```
* Le mode unique est utilisé lorsque les enregistrements font quelques secondes et que l'on veut qu'un seul spectrogramme par enregistrement *(mode __unique__, DURATION = durée de l'enregistrement et NB_IMG_PER_REC = 1)*
* Le mode multiple permet de découper l'enregistrements en plusieurs spectrogrammes de quelques secondes 

**WARNING** : Il est important de modifier la valeur de DURATION, OFFSET et NB_IMG_PER_REC dans le code (ligne 22 à 25)

```python
folder = 'Spectrogram/'
DURATION = 5
OFFSET = 2
if args.mode == 'multiple':
    NB_IMG_PER_REC = 10 
```

</details>


## Entrainement et Détection YOLO

* install YOLOV5

```bash
git clone https://github.com/ultralytics/yolov5
cd yolov5
pip install -r requirements.txt 
```
<details>
<summary>Entrainement</summary>
<p>
</p>

```bash
python3 train.py --img IMG_SIZE --batch BATCH_SIZE --EPOCHS NB_EPOCHS --data DIRECTION_OF_THE_RESULT/custom_data.yaml --weights yolov5/weights/yolov5s.pt --cache
```
</details>

<details>
<summary>Détection</summary>
<p>
</p>

* Sauvegarde les annotations en .txt ainsi que les images avec les bounding box dessus
```bash
python3 detect.py --weights ../psi-biom/YOLOV5/weights/best.pt --img 224 --conf 0.X --source PATH_TO_IMG --save-txt
```

**WARNING** : *--conf correspond à la confiance toléré par YOLO, c'est-à-dire à partir de quelle confiance d'une détection cette dernière est conservée, il faut donc modifier la valeur de X pour faire varier cette tolérence *(minimum : 0.0, maximum : 1)*

* Sauvegarde les annotations en .txt seulement avec la confiance de chaque détections

```bash
python3 detect.py --weights ../psi-biom/YOLOV5/weights/best.pt --img 224 --conf 0.X --source PATH_TO_IMG --save-txt --nosave --save-conf
``` 
</details>

<details>
<summary>Compilation des détections</summary>
<p>
</p>

```bash
cd ../
python3 get_yolo_detection.py -p PATH_TO_THE_TXT -d DIRECTION_OF_THE_RESULT #PATH_TO_THE_TXT ~ yolov5/runs/detect/expX/labels/
```
**WARNING** : Il est important d'ajouter sa propre liste de classes (ligne 79)

```python
#put the classes here
names = []
```

</details>

<details>
<summary>Conversion des détection YOLO vers LabelMe</summary>
<p>
</p>

```bash
pip install globox

globox convert yolov5/runs/detect/exp/labels/ DIRECTORY_TO_WICH_DATA_WILL_BE_STORED --format yolo --save_fmt labelme --img_folder PATH_TO_IMAGES
```

* Ce script permet de convertir les *.txt* en *.json* pour les ouvrir dans LabelMe mais il ne prend pas en compte le paramètre 'imageData'. Le script **get_json_file_YOLO.py** permet donc de récupérer ces données et les ajouter aux fichiers *.json*. 

```bash
python3 get_json_file_YOLO.py -p PATH_TO_STORED_DATA -i PATH_TO_IMAGES -d DIRECTORY_TO_WICH_DATA_WILL_BE_STORED
```

</details>

