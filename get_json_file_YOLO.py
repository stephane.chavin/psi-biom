import argparse
import os
import json
import labelme
import base64
import pandas as pd

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description='TODO')
parser.add_argument('-p','--path_to_json', type=str, help = 'Path of the folder that contain the .json',required=True)
parser.add_argument('-i','--path_to_img', type=str, help = 'Path of the folder that contain the .jpg',required=True)
parser.add_argument('-d','--direction', type=str, help = 'Directory to wich modified .json files will be stored',required=True)
args = parser.parse_args()


filename = args.path_to_json
out_file = args.direction
img_path = args.path_to_img

liste_file = os.listdir(filename) 
liste_file = pd.DataFrame(liste_file, columns =['fn'])

for i in range (len(liste_file)):
    if liste_file.fn[i][0] == '.':
        liste_file = liste_file.drop(i)

liste_file = liste_file.reset_index()

for i, row in liste_file.iterrows():
    if len(row.fn) > 30:
        data = labelme.LabelFile.load_image_file(str(img_path+row.fn[:-4]+'jpg'))
        image_data = base64.b64encode(data).decode('utf-8')
    else:
        continue
    try :
        len(data)
    except TypeError:
        continue
    f = open(filename+row.fn,)
    get_data = json.load(f)
    get_data['imageData'] = image_data
    get_data['imagePath'] = img_path+row.fn[:-4]+'jpg'
    with open(out_file+row.fn, 'w') as f:
        json.dump(get_data, f, indent=4)
