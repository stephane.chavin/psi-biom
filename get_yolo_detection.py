import pandas as pd
import os
import ipdb
from tqdm import tqdm
import argparse
from datetime import date

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter, description='TODO')
parser.add_argument('-p','--path_to_data', type=str, help = 'Path of the folder that contain the .txt files',required=True)
parser.add_argument('-d','--direction', type=str, help = 'Directory to wich the dataframe will be stored',required=True)
args = parser.parse_args()

annots = str(args.path_to_data)

today = date.today()
out_file = str('YOLO_detection_'+str('_'+today.day+'_'+today.month))

outdir = str(args.direction)

df = pd.DataFrame(columns = ['file','idx','espece','x','y','w','h','conf'])
for i in tqdm(os.listdir(annots)):
	if i == 'list_results_YOLO.csv':
		continue
	else:
		table = pd.read_csv(str(annots+i), sep=' ')
		l=[]
		tab= pd.DataFrame(columns = ['file','idx','espece','x','y','w','h','conf'])
		tab2 = pd.DataFrame(columns = ['file','idx','espece','x','y','w','h','conf'])

		if len (table) == 0:
			for j in table.columns:
				l.append(j)

			name = i.split('.')[0]

			if len(name.split('_')[-1]) == 2:
				name = name.split('.')[0][0:-3]
			elif len(name.split('_')[-1]) == 3:
				name = name.split('.')[0][0:-4]
			else :
				name= name.split('.')[0][0:-2]

			name = str(name+'.wav')
			idx = i.split('_')[-1]
			idx = idx.split('.')[0]

			table = pd.DataFrame([[name, idx, l[0],l[1],l[2],l[3],l[4],l[5]]], columns = ['file', 'idx','espece','x','y','w','h','conf'])
			df = pd.concat([df,table])
		else:
			nb = len(table)

			name = i.split('.')[0]

			if len(name.split('_')[-1]) == 2:
				name = name.split('.')[0][0:-3]
			elif len(name.split('_')[-1]) == 3:
				name = name.split('.')[0][0:-4]
			else :
				name= name.split('.')[0][0:-2]

			name = str(name+'.wav' )
			idx = i.split('_')[-1]
			idx = idx.split('.')[0]

			for j in table.columns:
				l.append(j)
			try :
				tab2 = pd.DataFrame([[name, idx, l[0],l[1],l[2],l[3],l[4],l[5]]],columns = ['file', 'idx','espece','x','y','w','h','conf'])
			except:
				ipdb.set_trace()
			for v in range(nb):
				new = pd.DataFrame([[name, idx, table.iloc[v][0],table.iloc[v][1], table.iloc[v][2],table.iloc[v][3],table.iloc[v][4],table.iloc[v][5]]], columns = ['file', 'idx','espece','x','y','w','h','conf'])
				tab = pd.concat([tab , new])

			table = pd.concat([tab2, tab])
			df = pd.concat([df, table])

#put the classes here
names = ['0:Homo sapiens',
  '1:Vulpes vulpes',
  '2:Meles meles',
  '3:Canis familiaris',
  '4:Sus scrofa',
  '5:voiture',
  '6:vélo',
  '7:Equus caballus',
  '8:Capreolus capreolus',
  '9:Rupicapra rupicapra',
  '10:Felis catus',
  '11:Lynx lynx',
  '12:Lepus europaeus',
  '13:camion',
  '14:Genetta genetta',
  '15:moto',
  '16:Felis silvestris',
  '17:Oryctolagus cuniculus',
  '18:Myocastor coypus',
  '19:Bos taurus',
  '20:Cervus elaphus',
  '21:Cinclus cinclus',
  '22:Capra hircus',
  '23:Martes martes',
  '24:Erinaceus europaeus',
  '25:Ardea cinerea',
  '26:train',
  '27:Sciurus vulgaris',
  '28:avion',
  '29:bus',
  '30:Eliomys quercinus',
  '31:Garrulus glandarius',
  '32:Ovis aries',
  '33:Natrix helvetica',
  '34:Salamandra salamandra',
  '35:Lutra lutra',
  '36:Motacilla cinerea',
  '37:Streptopelia turtur',
  '38:Phasianus colchicus',
  '39:Lacerta bilineata',
  '40:Testudo hermanni',
  '41:Equus asinus',
  '42:Ardea alba',
  '43:Buteo buteo',
  '44:Erithacus rubecula',
  '45:Natrix maura',
  '46:Rattus norvegicus']

df['annot'] = 'None'
for j in range (len(df)):
	df['annot'].iloc[j] = names[int(df.espece.iloc[j])]

df.to_csv(str(outdir+out_file+'.csv'), index= False)
